# GPT-Assistant: A RESTful Wrapper for OpenAI API
Welcome to the GitHub repository for GPT-Assistant, a RESTful service that interfaces with the OpenAI API to harness the features provided by the Generative Pre-trained Transformers (GPT).

This service capitalizes on the newly introduced function calling feature in the OpenAI API (Learn more about this update [here](https://openai.com/blog/function-calling-and-other-api-updates). Thus, allowing the user to specify an output format, dictating the structure expected in the returned answer.

Currently, GPT-Assistant is compatible with the following models:
- gpt-3.5-turbo
- gpt-4

Please visit the [OpenAI Pricing page](https://openai.com/pricing) to learn more about the costs associated with using these models.

## Requirements
- Docker 

## Installation
1. Clone the repository
```bash
cd GPT-Assistant
docker-compose up
```

## Individualization
1. Add your own API-Key to the application.yml.
2. src/main/resources/static to either change the input-data or the output-format.
3. Write your individual response models at de.fwojke.gptassistant.models.impl
4. Configure your request at de.fwojke.gptassistant.config.JsonConfiguration

## REST
Communicate with the REST-API via
```java
localhost:8080/api/gpt/ask
```