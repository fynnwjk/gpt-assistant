package de.fwojke.gptassistant.controller;

import de.fwojke.gptassistant.helper.MockDataProvider;
import de.fwojke.gptassistant.models.GptRequest;
import de.fwojke.gptassistant.models.impl.BlickfangResponse;
import de.fwojke.gptassistant.services.GptAssistantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/gpt")
public class GptApiController {

  @Autowired
  GptAssistantService gptAssistantService;

  @Autowired
  MockDataProvider mockDataProvider;

  @GetMapping("/showcase-blickfang-assistant")
  public BlickfangResponse showcase() throws Exception {
    String question = "Given the following payload consisting of sentences and the reading time (in ms) for each word, examine how good the reading performance is and give advices of the grammar tags in the dataset: ";
    return gptAssistantService.ask(question, mockDataProvider.provide());
  }

  // TODO
  @PostMapping("/ask")
  public BlickfangResponse ask(@RequestBody GptRequest gptRequest) {
    return new BlickfangResponse(0, null);
  }



}
