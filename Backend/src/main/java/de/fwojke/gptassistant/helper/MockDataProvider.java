package de.fwojke.gptassistant.helper;

import de.fwojke.gptassistant.config.JsonConfiguration;
import java.util.Map;
import java.util.Objects;
import org.apache.tomcat.util.json.JSONParser;
import org.apache.tomcat.util.json.ParseException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class MockDataProvider {
  public JSONObject provide() throws ParseException {
    String pathToMockData = "/static/mock-reading-data.json";
    JSONParser parser = new JSONParser(
        Objects.requireNonNull(JsonConfiguration.class.getResourceAsStream(pathToMockData)));

    Object parsed = parser.parse();
    if (parsed instanceof Map) {
      return new JSONObject((Map<?,?>) parsed);
    } else {
      throw new RuntimeException("Parsed JSON is not an object");
    }
  }
}
