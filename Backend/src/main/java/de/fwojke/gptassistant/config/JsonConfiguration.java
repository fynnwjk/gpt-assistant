package de.fwojke.gptassistant.config;

import de.fwojke.gptassistant.builder.JsonObjectBuilder;
import java.util.Map;
import java.util.Objects;
import org.apache.tomcat.util.json.JSONParser;
import org.apache.tomcat.util.json.ParseException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

/**
 * This class is responsible for configuring the JSON payload for OpenAI API requests.
 */
@Component
public class JsonConfiguration {
  // Path to the JSON function to be used in the payload
  private static final String PATH_TO_JSON_FUNCTION = "/static/blickfang-output-format.json";

  /**
   * Configures a JSON object for the API request payload.
   * Reads the default JSON structure from the file specified by PATH_TO_JSON_FUNCTION and populates it with the provided query.
   *
   * @param query the query string to be sent to the OpenAI API
   * @return a JSONObject ready to be sent as part of the API request payload
   * @throws JSONException if an error occurs while manipulating JSON objects
   * @throws RuntimeException if an error occurs while parsing the JSON file or if the parsed JSON is not an object
   */
  public JSONObject configure(final String query /* TODO */)
      throws JSONException {
    try  {
      JSONParser parser = new JSONParser(
          Objects.requireNonNull(JsonConfiguration.class.getResourceAsStream(PATH_TO_JSON_FUNCTION)));

      Object parsed = parser.parse();
      if (parsed instanceof Map) {
        JSONObject object = new JSONObject((Map<?,?>) parsed);

        JsonObjectBuilder builder = new JsonObjectBuilder();
        object = builder
            .withModel("gpt-3.5-turbo-0613")
            .withQuery(query)
            .withMaxTokens(1000)
            .withTemperature(0.0)
            .build(object);

        return object;
      } else {
        throw new RuntimeException("Parsed JSON is not an object");
      }
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }

}
