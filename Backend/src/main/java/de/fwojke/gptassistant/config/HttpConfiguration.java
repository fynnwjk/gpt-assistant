package de.fwojke.gptassistant.config;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * This class is responsible for configuring the HTTP connection for OpenAI API requests.
 */
@Component
public class HttpConfiguration {

  // The OpenAI API key for authentication.
  @Value("${openai.api.key}")
  private String apiKey;

  /**
   * Configures an HTTP connection to a specified URL with required headers for OpenAI API requests.
   *
   * @param url the URL to connect to
   * @return a configured HttpURLConnection
   * @throws IOException if an error occurs while establishing the connection
   */
  public HttpURLConnection configure(String url) throws IOException {
    HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
    con.setRequestMethod("POST");
    con.setRequestProperty("Content-Type", "application/json");
    con.setRequestProperty("Authorization", "Bearer " + apiKey);
    return con;
  }
}
