package de.fwojke.gptassistant.models;

import lombok.Data;

@Data
public class GptRequest {
  private String query;
  private String payload;
}
