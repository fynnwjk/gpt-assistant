package de.fwojke.gptassistant.models.impl;

import de.fwojke.gptassistant.models.GptRequest;
import java.util.ArrayList;
import java.util.Objects;
import lombok.Data;
import org.json.JSONArray;

/**
 * This class represents a response from the GPT-3.5 API.
 * It includes a total score and a list of suggestions (vorschlaege).
 */
@Data
public class BlickfangResponse extends GptRequest {
  private int totalScore;
  private ArrayList<String> vorschlaege;

  public BlickfangResponse(int totalScore, JSONArray vorschlaege) {
    Objects.requireNonNull(vorschlaege);
    this.totalScore = totalScore;
    this.vorschlaege = jsonToList(vorschlaege);
  }

  /**
   * Converts a JSONArray to an ArrayList.
   *
   * @param jsonArray the JSONArray to convert
   * @return the converted ArrayList
   */
  private ArrayList<String> jsonToList(final JSONArray jsonArray) {
    ArrayList<String> list = new ArrayList<>();
    for (int i = 0; i < jsonArray.length(); i++) {
      list.add(jsonArray.getString(i));
    }
    return list;
  }
}
