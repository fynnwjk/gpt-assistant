package de.fwojke.gptassistant.services;

import de.fwojke.gptassistant.builder.GptPromptBuilder;
import de.fwojke.gptassistant.config.HttpConfiguration;
import de.fwojke.gptassistant.config.JsonConfiguration;
import de.fwojke.gptassistant.models.impl.BlickfangResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for managing interactions with the GPT-3.5 API.
 * This service builds queries, sends them to the GPT-3.5 API, and processes the response.
 */
@Service
public class GptAssistantService {

  @Autowired
  HttpConfiguration httpConfiguration;

  @Autowired
  JsonConfiguration jsonConfiguration;

  // Base URL for the GPT-3.5 API. https://platform.openai.com/docs/api-reference/chat/create
  public static String URL = "https://api.openai.com/v1/chat/completions";

  /**
   * Sends a question to the GPT-3.5 API and returns the response.
   *
   * @param question the question to send to the GPT-3 API
   * @param payload the additional parameters for the GPT-3 API request
   * @return the response from the GPT-3.5 API
   * @throws Exception if an error occurs while sending the request or processing the response
   */
  public BlickfangResponse ask(final String question, final JSONObject payload) throws Exception {
    GptPromptBuilder builder = new GptPromptBuilder();
    final String query = builder
        .withQuestion(question)
        .withPayload(payload)
        .build();

    HttpURLConnection httpConnection = httpConfiguration.configure(URL);
    JSONObject jsonRequest = jsonConfiguration.configure(query);

    String output = makeRequest(httpConnection, jsonRequest);
    return answer(output);
  }

  /**
   * Sends a request to the specified HTTP connection and returns the response.
   *
   * @param httpConnection the HTTP connection to send the request to
   * @param jsonRequest the JSON object representing the request body
   * @return the response from the server
   * @throws IOException if an error occurs while sending the request or reading the response
   */
  private String makeRequest(final HttpURLConnection httpConnection, final JSONObject jsonRequest)
      throws IOException {
    httpConnection.setDoOutput(true);
    httpConnection.getOutputStream().write(jsonRequest.toString().getBytes());
    return new BufferedReader(new InputStreamReader(httpConnection.getInputStream())).lines()
        .reduce((a, b) -> a + b).get();
  }

  /**
   * Processes the API output and returns a GptResponse object.
   *
   * @param apiOutput the raw API output
   * @return a GptResponse object representing the API output
   */
  private BlickfangResponse answer(final String apiOutput) {
    JSONObject jsonResponse = extractArgumentsFromJsonObject(apiOutput);
    int totalScore = jsonResponse.getInt("totalScore");
    JSONArray vorschlaege = jsonResponse.getJSONArray("vorschlaege");
    return new BlickfangResponse(totalScore, vorschlaege);
  }

  /**
   * Extracts relevant information from the API output JSON.
   *
   * @param output the raw API output
   * @return a JSONObject containing the extracted information
   */
  private JSONObject extractArgumentsFromJsonObject(final String output) {
    JSONArray jsonArray = new JSONObject(output).getJSONArray("choices");
    JSONObject message = jsonArray.getJSONObject(0).getJSONObject("message");
    JSONObject functionCall = message.getJSONObject("function_call");
    return new JSONObject(functionCall.getString("arguments"));
  }

}
