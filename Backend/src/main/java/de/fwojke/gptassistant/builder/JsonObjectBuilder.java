package de.fwojke.gptassistant.builder;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * This class is responsible for building JSONObjects used in OpenAI API requests.
 * It provides a fluent API for setting model, query, max tokens, and temperature.
 */
public class JsonObjectBuilder {
    private String model;
    private String query;
    private Integer maxTokens;
    private Double temperature;

    public JsonObjectBuilder withModel(String model) {
      this.model = model;
      return this;
    }

    public JsonObjectBuilder withQuery(String query) {
      this.query = query;
      return this;
    }

    public JsonObjectBuilder withMaxTokens(Integer maxTokens) {
      this.maxTokens = maxTokens;
      return this;
    }

    public JsonObjectBuilder withTemperature(Double temperature) {
      this.temperature = temperature;
      return this;
    }

    public JSONObject build(final JSONObject jsonObject) {
      if (temperature != null && !temperature.isNaN()) {
        jsonObject.put("temperature", temperature);
      }
      if (query != null) {
        JSONObject messageObject = new JSONObject();
        messageObject.put("role", "user");
        messageObject.put("content", query);
        JSONArray messagesArray = new JSONArray();
        messagesArray.put(messageObject);
        jsonObject.put("messages", messagesArray);
      }
      if (maxTokens != null) {
        jsonObject.put("max_tokens", maxTokens);
      }
      if (model != null && !model.isBlank()) {
        jsonObject.put("model", model);
      }
      return jsonObject;
    }

}
