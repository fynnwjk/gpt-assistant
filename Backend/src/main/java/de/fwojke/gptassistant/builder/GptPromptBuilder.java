package de.fwojke.gptassistant.builder;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is responsible for constructing prompts for the OpenAI API.
 * It provides a fluent API for setting question and payload, and computes the estimated cost of the prompt.
 */
public class GptPromptBuilder {
  final static Logger logger = LoggerFactory.getLogger(GptPromptBuilder.class);

  private String question;
  private String payload;

  /**
   * Sets the question part of the prompt.
   *
   * @param question the question string
   * @return this builder
   */
  public GptPromptBuilder withQuestion(String question) {
    this.question = question;
    return this;
  }

  /**
   * Sets the payload part of the prompt.
   *
   * @param payload the payload JSONObject
   * @return this builder
   */
  public GptPromptBuilder withPayload(JSONObject payload) {
    this.payload = minifyJSONPayload(payload);
    return this;
  }

  /**
   * Constructs the prompt and logs the estimated cost of the prompt.
   *
   * @return the constructed prompt string
   */
  public String build() {
    final String prompt = question + payload;
    final int tokenCount = getTokenCount(prompt);
    final double pricePer1kTokens = 0.0015;
    final BigDecimal estimatedPrice = BigDecimal
        .valueOf(tokenCount)
        .divide(BigDecimal.valueOf(1000), RoundingMode.HALF_UP)
        .multiply(BigDecimal.valueOf(pricePer1kTokens));
    logger.info("\n Token count for this prompt: " + tokenCount + "\n GPT-3.5 0.0015$/1K Tokens (June 2023): ~" + estimatedPrice + "$");
    return prompt;
  }

  /**
   * Minifies the payload JSON object for inclusion in the prompt.
   *
   * @param payload the payload JSONObject
   * @return the minified payload string
   */
  public String minifyJSONPayload(JSONObject payload) {
    return payload.toString();
  }

  /**
   * Counts the number of tokens in the input text.
   *
   * @param text The input text.
   * @return The count of tokens in the text.
   */
  public static int getTokenCount(String text) {
    int count = 0;
    Matcher m = Pattern.compile("\\b\\w+\\b|\\S").matcher(text);
    while (m.find()) {
      count++;
    }
    return count;
  }

}
