package de.fwojke.gptassistant;

import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GptAssistantApplication {

	public static void main(String[] args) {
		SpringApplication.run(GptAssistantApplication.class, args);
	}

}
