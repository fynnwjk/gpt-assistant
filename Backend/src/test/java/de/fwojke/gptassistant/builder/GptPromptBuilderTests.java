package de.fwojke.gptassistant.builder;

import static org.junit.Assert.assertEquals;

import de.fwojke.gptassistant.config.JsonConfiguration;
import java.util.Map;
import java.util.Objects;
import org.apache.tomcat.util.json.JSONParser;
import org.apache.tomcat.util.json.ParseException;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class GptPromptBuilderTests {
  private static final String PATH_TO_MOCK_DATA = "/static/mock-reading-data.json";

  @Test
  void testPromptBuilder() throws ParseException {
    GptPromptBuilder builder = new GptPromptBuilder();
    String prompt = builder.withQuestion("")
        .withPayload(mockJsonData())
        .build();
    Assertions.assertNotNull(prompt);
  }

  private JSONObject mockJsonData() throws ParseException {
    JSONParser parser = new JSONParser(
        Objects.requireNonNull(JsonConfiguration.class.getResourceAsStream(PATH_TO_MOCK_DATA)));

    Object parsed = parser.parse();
    if (parsed instanceof Map) {
      return new JSONObject((Map<?,?>) parsed);
    } else {
      throw new RuntimeException("Parsed JSON is not an object");
    }
  }
}
