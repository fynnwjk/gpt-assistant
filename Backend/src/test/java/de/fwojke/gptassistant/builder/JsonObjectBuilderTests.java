package de.fwojke.gptassistant.builder;

import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class JsonObjectBuilderTests {

  @Test
  public void testJsonObjectBuilder() {
    JsonObjectBuilder builder = new JsonObjectBuilder();
    JSONObject jsonObject = new JSONObject();
    jsonObject = builder
        .withModel("gpt-123")
        .withTemperature(0.0)
        .withMaxTokens(0)
        .build(jsonObject);
    Assertions.assertNotNull(jsonObject);
  }

}
