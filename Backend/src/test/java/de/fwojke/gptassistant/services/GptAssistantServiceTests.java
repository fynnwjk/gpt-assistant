package de.fwojke.gptassistant.services;

import de.fwojke.gptassistant.models.impl.BlickfangResponse;
import java.util.Map;
import java.util.Objects;
import org.apache.tomcat.util.json.JSONParser;
import org.apache.tomcat.util.json.ParseException;
import org.json.JSONObject;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GptAssistantServiceTests {

  @Autowired
  GptAssistantService gptAssistantService;

  private static final String PATH_TO_MOCK_DATA = "/static/mock-reading-data.json";

  @Test
  @Disabled // Execution costs $.
  void testService() throws Exception {
    String question = "Given the following payload consisting of sentences and the reading time (in ms) for each word, examine how good the reading performance is and give advices of the grammar tags in the dataset: ";
    BlickfangResponse response = gptAssistantService.ask(question, mockJsonData());

    System.out.println(response);
  }

  /**
   * This method is used to parse JSON data from a mock data file.
   * The parsed data is then returned as a JSONObject.
   *
   * @return a JSONObject representing the parsed mock data.
   * @throws ParseException if there is an error during parsing the mock data.
   * @throws RuntimeException if the parsed JSON data is not a valid JSON object.
   */
  private JSONObject mockJsonData() throws ParseException {
    JSONParser parser = new JSONParser(
        Objects.requireNonNull(GptAssistantServiceTests.class.getResourceAsStream(PATH_TO_MOCK_DATA)));

    Object parsed = parser.parse();
    if (parsed instanceof Map) {
      return new JSONObject((Map<?,?>) parsed);
    } else {
      throw new RuntimeException("Parsed JSON is not an object");
    }
  }
}
